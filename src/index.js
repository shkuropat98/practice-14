import Card from './Card.js';
import Game from "./Game.js";
import SpeedRate from "./SpeedRate.js";

class Creature extends Card{
    getDescriptions() {
        return [super.getDescriptions(this), getCreatureDescription(this) ]
    }
}
class Duck extends Creature {
    constructor() {
        super(`Мирная утка`, 2);
    }
    quacks() {
        console.log('quack')
    }
    swims() {
        console.log('float: both;')
    }
}
class Dog extends Creature {
    constructor(name = 'Пес-бандит', maxPower = 3 ) {
        super(name, maxPower);
    }
    swims() {
        console.log('float: none;')
    };
}
class Lad extends Dog {
    //Добавьте в описание карты «Чем их больше, тем они сильнее».
    static getInGameCount() {
        return this.inGameCount || 0
    }
    static setInGameCount(value) {
        this.inGameCount = value;
    }
    static getBonus(){
        return {
            bonusAttack: Lad.getInGameCount() * (Lad.getInGameCount() + 1) / 2,
            bonusDefense: Lad.getInGameCount() * (Lad.getInGameCount() + 1) / 2
        }
    }
    constructor() {
        super(`Браток`, 2);
    }
    modifyDealedDamageToCreature(value, toCard, gameContext, continuation) {
        let {bonusAttack} = Lad.getBonus();
        let newValue =  bonusAttack
        console.log(newValue)
        super.modifyDealedDamageToCreature(newValue, toCard, gameContext, continuation)
    }

    modifyTakenDamage(value, fromCard, gameContext, continuation) {
    let {bonusDefense} = Lad.getBonus();
    let newValue = bonusDefense;
    console.log(newValue)
    super.modifyTakenDamage(newValue, fromCard, gameContext, continuation)
    }

    doAfterComingIntoPlay(gameContext, continuation) {
        let newCount =  Lad.getInGameCount() + 1;
        Lad.setInGameCount(newCount)
        super.doAfterComingIntoPlay(gameContext, continuation)
    }
    doBeforeRemoving(continuation) {
        let newCount = Lad.getInGameCount() - 1;
        Lad.setInGameCount(newCount)
        super.doBeforeRemoving(continuation)
    }
}

// Дает описание существа по схожести с утками и собаками
function getCreatureDescription(card) {
    if (isDuck(card) && isDog(card)) {
        return 'Утка-Собака';
    }
    if (isDuck(card)) {
        return 'Утка';
    }
    if (isLad(card)) {
        return 'Чем их больше, тем они сильнее'
    }
    if (isDog(card)) {
        return 'Собака';
    }
    return 'Существо';
}

// Отвечает является ли карта уткой.
function isDuck(card) {
    return card && card.swims && card.quacks
}
// Отвечает является ли карта собакой.
function isDog(card) {
    return card instanceof Dog;
}
function isLad(card) {
    return Lad.prototype.hasOwnProperty('modifyDealedDamageToCreature') && card instanceof Lad
}

// Колода Шерифа, нижнего игрока.
const seriffStartDeck = [
     new Duck(),
     new Duck(),
     new Duck(),
]

// Колода Бандита, верхнего игрока.
const banditStartDeck = [
    new Lad(),
    new Lad()
];

// Создание игры.
const game = new Game(seriffStartDeck, banditStartDeck);

// Глобальный объект, позволяющий управлять скоростью всех анимаций.
SpeedRate.set(1);

// Запуск игры.
game.play(false, (winner) => {
    alert('Победил ' + winner.name);
});
